OS = $(word 2,$(subst -, ,$(CMTCONFIG)))

MANIFEST = InstallArea/$(CMTCONFIG)/manifest.xml

REQUIREMENTS_TGTS = LHCbGridConfig/cmt/requirements LHCbGridSys/cmt/requirements


RUNIT_TOOLS = runit runit-init runsv runsvchdir runsvctrl runsvdir runsvstat chpst

PYTHON_VERSION_TWODIGIT=27
LCG_VER=2015-01-09
ifeq ($(OS),slc6)
  DIRACPLAT=Linux_x86_64_glibc-2.12
endif
DIRAC_BUNDLE=$(LHCBTAR)/DIRAC3/lcgBundles/DIRAC-lcg-$(LCG_VER)-$(DIRACPLAT)-python$(PYTHON_VERSION_TWODIGIT).tar.gz


all: $(MANIFEST) requirements runit_tools

requirements: $(REQUIREMENTS_TGTS)
	

$(MANIFEST): LHCbGridConfig/$(OS).manifest.xml
	echo '# Building package LHCbGrid [0/0]' > build.$(CMTCONFIG).log
	echo 'dummy log' >> build.$(CMTCONFIG).log
	mkdir -p `dirname $@`
	sed 's/BINARY_TAG/$(CMTCONFIG)/g' $< > $@

LHCbGridConfig/cmt/requirements: $(wildcard LHCbGridConfig/*.manifest.xml)  LHCbGridConfig/cmt/requirements.tpl
	python LHCbGridConfig/gen_requirements.py

LHCbGridSys/cmt/requirements: LHCbGridConfig/cmt/requirements LHCbGridSys/cmt/requirements.tpl
	sed s'/PROJECT_VERSION/'`awk '/^ *version/{print $$2; exit}' LHCbGridConfig/cmt/requirements`'/g' LHCbGridSys/cmt/requirements.tpl > LHCbGridSys/cmt/requirements

runit_tools:
	mkdir -p InstallArea/$(CMTCONFIG)/bin
	tar -x -v -f $(DIRAC_BUNDLE) --xform='s#.*/\([^/]\+\)#InstallArea/$(CMTCONFIG)/bin/\1#' --show-transformed-names $(patsubst %,*/%,$(RUNIT_TOOLS))

clean:
	$(RM) $(MANIFEST) $(REQUIREMENTS_TGTS) $(patsubst %,InstallArea/$(CMTCONFIG)/bin/%,$(RUNIT_TOOLS))
purge: clean
	$(RM) -r InstallArea/$(CMTCONFIG)

# fake targets to respect the interface of the Gaudi wrapper to CMake
configure:
install:
unsafe-install:
post-install:
