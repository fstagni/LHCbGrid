package LHCbGridConfig
version ${version}

${macros}

set LCG_GFAL_VO lhcb
set LCG_GFAL_INFOSYS lcg-bdii.cern.ch:2170
set GFAL_PLUGIN_DIR $$(LCG_GridExternal)/Grid/gfal2/$$(gfal2_config_version)/$$(LCG_system)/lib64/gfal2-plugins/

set STAGE_HOST "$$(STAGE_HOST)" CERN "castorlhcb"
set CSEC_NOTHREAD 0
set RFIO_USE_CASTOR_V2 "YES"
set GLOBUS_GSSAPI_FORCE_TLS 1
set XrdSecSSLSESSION 0
