package LHCbGridSys
version PROJECT_VERSION


use pygraphics v* LCG_Interfaces
use Qt v* LCG_Interfaces
use pygsi v* LCG_Interfaces
use pyxrootd v* LCG_Interfaces

use voms v* LCG_Interfaces
use epel v* LCG_Interfaces

use gridsite v* LCG_Interfaces
use fts v* LCG_Interfaces
use fts3 v* LCG_Interfaces
use lcgutils v* LCG_Interfaces
use gfal v* LCG_Interfaces
use gfal2 v* LCG_Interfaces
use gridftp_ifce v* LCG_Interfaces
use srm_ifce v* LCG_Interfaces
use is_ifce v* LCG_Interfaces
use dcap v* LCG_Interfaces

use lcgdmcommon v* LCG_Interfaces
use canl v* LCG_Interfaces
use lb v* LCG_Interfaces


macro use_CASTOR "" \
      LHCbGrid   " CASTOR v* LCG_Interfaces " \
      CERN       " CASTOR v* LCG_Interfaces "

use $(use_CASTOR)

macro use_dpm "" \
      LHCbGrid   " dpm v* LCG_Interfaces "

use $(use_dpm)

macro use_dcache_client "" \
      LHCbGrid   " dcache_client v* LCG_Interfaces "

use $(use_dcache_client)

use tcmalloc v* LCG_Interfaces
use libtool v* LCG_Interfaces

#-------------------------------------------------------------------
path_prepend PATH $(LHCBGRID_home)/$(cmt_installarea_prefix)/$(CMTCONFIG)/bin

use LHCbGridConfig PROJECT_VERSION
