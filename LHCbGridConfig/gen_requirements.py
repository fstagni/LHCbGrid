#!/usr/bin/env python

import sys
import os
from xml.etree import ElementTree as ET
from collections import defaultdict

base = os.path.dirname(__file__)
os.chdir(base)

manifests = [(name.replace('.manifest.xml', ''), ET.parse(name))
             for name in os.listdir(os.curdir)
             if name.endswith('.manifest.xml')]
if not manifests:
    print 'Error: no manifexst.xml template in', base
    sys.exit(1)

# {rpm_name: interface_name}
interface_mapping = {'FTS': 'fts',
                     'FTS3': 'fts3',
                     'dm-util': 'lcgutils',
                     'is-ifce': 'is_ifce',
                     'gridftp-ifce': 'gridftp_ifce',
                     'srm-ifce': 'srm_ifce',
                     'DPM': 'dpm',
                     'lcg-dm-common': 'lcgdmcommon'
                     }

# FIXME: we need a better way to extract the version of LHCbGrid
proj_version = manifests[0][1].find('project').attrib['version']

# fill up a dictionary with all the required externals with the versions
# per platform
externals = defaultdict(dict)
for platform, manifest in manifests:
    for ext in manifest.findall('exttools/packages/package'):
        name = ext.attrib['name']
        version = ext.attrib['version']
        externals[name][platform] = version

macros = []
for name in sorted(externals):
    macro_name = '%s_config_version' % interface_mapping.get(name, name)
    # convert the list of versions to the CMT macro syntax:
    # default_value [tag1 value1 [...]]
    alternatives = ['target-%s "%s"' % (platform, version)
                    for platform, version in externals[name].items()]
    alternatives.insert(0, '"$(%s)"' % macro_name)
    macro_values = ' \\\n    '.join(alternatives)
    macros.append('macro %s %s\n' % (macro_name, macro_values))

from string import Template
tpl = Template(open(os.path.join('cmt', 'requirements.tpl')).read())
f = open(os.path.join('cmt', 'requirements'), 'w')
f.write(tpl.substitute(version=proj_version, macros='\n'.join(macros)))
f.close()
